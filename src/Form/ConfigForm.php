<?php

namespace Drupal\tg_integration\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tg_integration\TgIntegrationHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures forms module settings.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module helper.
   *
   * @var \Drupal\tg_integration\TgIntegrationHelper
   */
  protected $helper;

  /**
   * The tg_integration config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object. Injects services.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface|null $typedConfigManager
   *   The typed configuration manager service.
   * @param \Drupal\tg_integration\TgIntegrationHelper $helper
   *   The extension path resolver.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   The extension path resolver.
   */
  public function __construct(ConfigFactoryInterface $configFactory, $typedConfigManager, TgIntegrationHelper $helper, ExtensionPathResolver $extension_path_resolver) {
    parent::__construct($configFactory, $typedConfigManager);
    $this->helper = $helper;
    $this->extensionPathResolver = $extension_path_resolver;
    $this->config = $this->configFactory->get('tg_integration.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
        $container->get('config.typed') ?? NULL,
      $container->get('tg_integration.helper'),
      $container->get('extension.path.resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tg_integration_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'tg_integration.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $link = '/' . $this->extensionPathResolver->getPath('module', 'tg_integration') . '/README.md';
    $entity_options = $this->helper->getEntitiesOptions();

    $form['api_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Telegram API settings'),
      '#open' => TRUE,
    ];
    $form['api_settings']['chat_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Chat name / Chat ID'),
      '#default_value' => $this->config->get('chat_name'),
      '#placeholder' => 'chat_name',
      '#description' => $this->t(
        'Channel name or chat id in which post should be posted (symbols after t.me/ in the channel link. Or numbers from chat message link (without trailing -100). See details in the module <a href=":link" target="_blank">README.md</a> file.',
        [':link' => $link]
      ),
    ];
    $form['api_settings']['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bot authentication token'),
      '#default_value' => $this->config->get('token'),
      '#placeholder' => '123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11',
      '#description' => $this->t(
        'Token to access the HTTP API. Please, <a href=":link" target="_blank">create</a> bot before and add it to the channel as administrator.',
        [':link' => 'https://core.telegram.org/bots#creating-a-new-bot']
      ),
    ];
    $form['post_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Default post options'),
      '#open' => TRUE,
    ];
    $form['post_settings']['display_tg_options'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display Options on the entity edit form'),
      '#default_value' => $this->config->get('display_tg_options'),
      '#description' => $this->t('Display Options for post to the Telegram channel on the each entity edit form. Otherwise default options from this form will be used.'),
    ];
    $form['post_settings']['post_to_tg'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Post entity to the Telegram chanel'),
      '#default_value' => $this->config->get('post_to_tg'),
      '#description' => $this->t('Post newly created entities to the Telegram chanel. You can change this option for each entity on the edit form.'),
    ];
    $form['post_settings']['display_comments'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display comments block'),
      '#default_value' => $this->config->get('display_comments'),
      '#description' => $this->t('Display Telegram comments widget for the newly created entities. You can change this option for each entity on the edit form. NOTE: Current option not worked for private channels or groups.'),
    ];
    $form['post_settings']['silent_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Silent mode'),
      '#default_value' => $this->config->get('silent_mode'),
      '#description' => $this->t('Send post to the Telegram chanel in silent mode (without sound notification).'),
    ];
    $form['post_settings']['disable_link_preview'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Disable preview for links in the post"),
      '#default_value' => $this->config->get('disable_link_preview'),
      '#description' => $this->t("Display only link in the post. Without image and teaser text from the site's Open Graph meta tags."),
    ];
    $form['content_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Content integrations settings'),
      '#description' => $this->t('Choose content entities for posting to the Telegram channel.'),
      '#open' => TRUE,
    ];
    $form['content_settings']['content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content Types'),
      '#options' => $entity_options,
      '#empty_option' => $this->t('- Select an existing content type -'),
      '#default_value' => $this->config->get('content_types') ?: [],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $display_comments = is_numeric($values['chat_name']) ? FALSE : $values['display_comments'];
    $this->config('tg_integration.settings')
      ->set('chat_name', $values['chat_name'])
      ->set('token', $values['token'])
      ->set('display_tg_options', $values['display_tg_options'])
      ->set('post_to_tg', $values['post_to_tg'])
      ->set('display_comments', $display_comments)
      ->set('silent_mode', $values['silent_mode'])
      ->set('disable_link_preview', $values['disable_link_preview'])
      ->set('content_types', $values['content_types'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
