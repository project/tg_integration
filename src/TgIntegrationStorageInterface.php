<?php

namespace Drupal\tg_integration;

/**
 * Provides an interface defining tg_integration Storage.
 *
 * Stores data about entity and the corresponding post in the Telegram channel.
 */
interface TgIntegrationStorageInterface {

  /**
   * Writes data about the post to the database.
   *
   * @param string $entity_type
   *   The type of the entity which posted to the Telegram.
   * @param int $entity_id
   *   The ID of the entity which posted to the Telegram.
   * @param int $tg_post_id
   *   The message_id in the response from the Telegram.
   * @param bool $display_comments
   *   TRUE (default) to display Telegram comments block under the node;
   *   FALSE to disable Telegram comments block under the node.
   *
   * @return bool
   *   TRUE if the data about the post has been written to the database.
   *
   * @see https://core.telegram.org/bots/api#message
   */
  public function setTgPost(
    string $entity_type,
    int $entity_id,
    int $tg_post_id,
    bool $display_comments = TRUE,
  ): bool;

  /**
   * Gets the Post id in the Telegram (message_id from request) for entity.
   *
   * @param string $entity_type
   *   The type of the entity which posted to the Telegram.
   * @param int $entity_id
   *   The ID of the entity which posted to the Telegram.
   *
   * @return int|null
   *   The post id in the Telegram (message_id from request) for current entity.
   *
   * @see https://core.telegram.org/bots/api#message
   */
  public function getTgPostId(string $entity_type, int $entity_id): ?int;

  /**
   * Checks if the current entity has been published to the Telegram chanel.
   *
   * @param string $entity_type
   *   The type of the entity being checked.
   * @param int $entity_id
   *   The ID of the entity being checked.
   *
   * @return bool
   *   TRUE if entity has been posted to the Telegram chanel;
   *   FALSE if entity hasn't been posted to the Telegram chanel;
   */
  public function hasTgPost(string $entity_type, int $entity_id): bool;

  /**
   * Gets path for the post in the Telegram for the entity.
   *
   * Gets path in format 'chanel_name/post_id'. F.e. 'finews_ua/56'.
   *
   * @param string $entity_type
   *   The type of the entity which posted to the Telegram.
   * @param int $entity_id
   *   The ID of the entity which posted to the Telegram.
   *
   * @return string|null
   *   The path for the post in the Telegram for the entity in format
   *   'chanel_name/post_id'. NULL if the current entity hasn't been posted to
   *   the Telegram chanel.
   */
  public function getTgPostPath(string $entity_type, int $entity_id): ?string;

  /**
   * Gets status display Telegram comments block under the node.
   *
   * @param string $entity_type
   *   The type of the entity which posted to the Telegram.
   * @param int $entity_id
   *   The ID of the entity which posted to the Telegram.
   *
   * @return bool|null
   *   TRUE to display Telegram comments block under the node;
   *   FALSE to disable Telegram comments block under the node;
   *   NULL if the current entity hasn't been posted to the Telegram chanel.
   */
  public function getCommentsStatus(string $entity_type, int $entity_id): ?bool;

  /**
   * Changes whether display Telegram comments block under the node or not.
   *
   * @param string $entity_type
   *   The type of the entity which posted to the Telegram.
   * @param int $entity_id
   *   The ID of the entity which posted to the Telegram.
   * @param bool $display_comments
   *   TRUE to display Telegram comments block under the node;
   *   FALSE to disable Telegram comments block under the node.
   *
   * @return bool
   *   TRUE if the data about comments displaying has been changed.
   */
  public function changeCommentsStatus(
    string $entity_type,
    int $entity_id,
    bool $display_comments,
  ): bool;

}
