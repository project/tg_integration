<?php

namespace Drupal\tg_integration;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\State\StateInterface;

/**
 * Provides the default database storage backend for tg_integration.
 */
class TgIntegrationStorage implements TgIntegrationStorageInterface {

  /**
   * The database connection used.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs the tg_integration storage.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection for the tg_integration storage.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(Connection $connection, StateInterface $state, ConfigFactoryInterface $config_factory) {
    $this->connection = $connection;
    $this->state = $state;
    $this->configFactory = $config_factory;
    $this->config = $this->configFactory->get('tg_integration.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function setTgPost(
    string $entity_type,
    int $entity_id,
    int $tg_post_id,
    bool $display_comments = TRUE,
  ): bool {
    return (bool) $this->connection
      ->merge('tg_integration')
      ->keys(['entity_type', 'entity_id'], [$entity_type, $entity_id])
      ->fields([
        'entity_type' => $entity_type,
        'entity_id' => $entity_id,
        'tg_chanel_name' => $this->config->get('chat_name'),
        'tg_post_id' => $tg_post_id,
        'display_comments' => (int) $display_comments,
      ])
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getTgPostId(string $entity_type, int $entity_id): ?int {
    $query = $this->connection
      ->select('tg_integration', 'tg')
      ->fields('tg', ['tg_post_id'])
      ->condition('entity_type', $entity_type)
      ->condition('entity_id', $entity_id)
      ->execute()
      ->fetchAll();
    if ($query) {
      foreach ($query as $data) {
        return $data->tg_post_id;
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function hasTgPost(string $entity_type, int $entity_id): bool {
    $query = $this->connection
      ->select('tg_integration', 'tg')
      ->fields('tg', ['tg_post_id'])
      ->condition('entity_type', $entity_type)
      ->condition('entity_id', $entity_id)
      ->execute()
      ->fetchAll();
    if ($query) {
      foreach ($query as $data) {
        if ($data->tg_post_id > 0) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getTgPostPath(string $entity_type, int $entity_id): ?string {
    $query = $this->connection
      ->select('tg_integration', 'tg')
      ->fields('tg', ['tg_chanel_name', 'tg_post_id'])
      ->condition('entity_type', $entity_type)
      ->condition('entity_id', $entity_id)
      ->execute()
      ->fetchAll();
    if ($query) {
      foreach ($query as $data) {
        return $data->tg_chanel_name . '/' . $data->tg_post_id;
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommentsStatus(string $entity_type, int $entity_id): ?bool {
    $query = $this->connection
      ->select('tg_integration', 'tg')
      ->fields('tg', ['display_comments'])
      ->condition('entity_type', $entity_type)
      ->condition('entity_id', $entity_id)
      ->execute()
      ->fetchAll();
    if ($query) {
      foreach ($query as $data) {
        return $data->display_comments;
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function changeCommentsStatus(
    string $entity_type,
    int $entity_id,
    bool $display_comments,
  ): bool {
    return (bool) $this->connection
      ->merge('tg_integration')
      ->keys(['entity_type', 'entity_id'], [$entity_type, $entity_id])
      ->fields(['display_comments' => (int) $display_comments])
      ->execute();
  }

}
