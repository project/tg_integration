<?php

namespace Drupal\tg_integration;

/**
 * Defines a Bot interface for the module 'Telegram integration'.
 */
interface TgIntegrationBotInterface {

  const BASE_TG_URL = 'https://api.telegram.org/bot';

  /**
   * Sends a text messages to the Telegram channel.
   *
   * @param array $parameters
   *   The array of parameters. 'chat_id' not needed.
   *
   * @return int|null
   *   The message_id from 'Message' object
   *
   * @see https://core.telegram.org/bots/api#sendmessage
   * @see https://core.telegram.org/bots/api#message
   */
  public function sendMessage(array $parameters): ?int;

  /**
   * Gets the number of members in the Telegram chat. Returns Int on success.
   *
   * @return int
   *   The amount of the chanel's subscribers.
   *
   * @see https://core.telegram.org/bots/api#getchatmembercount
   */
  public function getChatMemberCount(): int;

  /**
   * Sends a request to the Telegram bot API.
   *
   * @param string $command
   *   The string with API method name.
   * @param array $parameters
   *   The array of parameters fot the method. 'chat_id' not needed.
   *
   * @return array
   *   The array with response.
   *
   *  @see https://core.telegram.org/bots/api
   */
  public function doRequest(string $command, array $parameters = []): array;

}
