<?php

namespace Drupal\tg_integration;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Error;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;

/**
 * Sending commands to the Telegram Bot.
 */
class TgIntegrationBot implements TgIntegrationBotInterface {

  use StringTranslationTrait;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * Constructs a new TgIntegrationBot object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    MessengerInterface $messenger,
    LoggerInterface $logger,
    ClientInterface $http_client,
  ) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->logger = $logger;
    $this->httpClient = $http_client;
    $this->config = $this->configFactory->get('tg_integration.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function sendMessage(array $parameters): ?int {
    $response = $this->doRequest('sendMessage', $parameters);
    return $response['result']['message_id'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getChatMemberCount(): int {
    $response = $this->doRequest('getChatMemberCount');
    return $response['result'];
  }

  /**
   * {@inheritdoc}
   */
  public function doRequest(string $command, array $parameters = []): array {
    $token = $this->config->get('token');
    $chat_name = $this->config->get('chat_name');
    if (!$token || !$chat_name) {
      $link = Link::createFromRoute('settings', 'tg_integration.settings')->toString();
      $this->messenger->addWarning($this->t('Request to Telegram was not done. Please fill @settings with correct data.', ['@settings' => $link]));
      return [];
    }

    $chat_prefix = is_numeric($chat_name) ? '-100' : '@';

    $request_url = self::BASE_TG_URL . $token . '/' . $command;
    $options = ['query' => $parameters];
    $options['query']['chat_id'] = $chat_prefix . $chat_name;
    try {
      $response = $this->httpClient->request('GET', $request_url, $options);
    }
    catch (GuzzleException $e) {
      $this->logger->error(
        'Telegram integration encountered an error when send request to the url: @url. @message in %function (line %line of %file).',
        Error::decodeException($e) + ['@url' => $request_url]);
      $this->messenger->addWarning($this->t('Entity has not been posted to Telegram. View logs for details.'));
      return [];
    }
    if ($response->getStatusCode() == 200) {
      return json_decode($response->getBody()->getContents(), TRUE);
    }
    return [];
  }

}
