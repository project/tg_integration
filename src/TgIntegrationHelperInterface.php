<?php

namespace Drupal\tg_integration;

use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a helper interface for module 'Telegram integration'.
 */
interface TgIntegrationHelperInterface {

  /**
   * Returns a list of content entity types.
   *
   * Returns a list of the currently installed content entity types,
   * except some 'internal' types.
   *
   * @return array
   *   An array of content types.
   */
  public function getContentEntities():array;

  /**
   * Returns a result of comparison the type and bundle of the current entity.
   *
   * Returns a result of comparison the type and bundle of the current entity
   * with the selected ones in the module's settings.
   *
   * @param string $entity_type
   *   The entity type (for example, node) for comparison.
   * @param string $bundle
   *   The entity bundle (for example 'article', 'page').
   *
   * @return bool
   *   TRUE if the current entity type and bundle is selected in the settings.
   */
  public function isCheckedEntity(string $entity_type, string $bundle): bool;

  /**
   * Returns an array of checked entities.
   *
   * Returns an array of the entities, selected in the module's settings.
   *
   * @return array
   *   An array of the checked entities.
   */
  public function getCheckedEntities(): array;

  /**
   * Returns a list of all the content entity types, currently installed.
   *
   * Result is prepared for using as options in checkboxes.
   *
   * @return array
   *   An array of content types.
   */
  public function getEntitiesOptions(): array;

  /**
   * Returns a result of processing entity on presave.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   *
   * @return bool
   *   TRUE if success; FALSE otherwise.
   */
  public function processEntity(EntityInterface $entity): bool;

}
