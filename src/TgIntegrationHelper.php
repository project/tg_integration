<?php

namespace Drupal\tg_integration;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * A helper for module 'Telegram integration'.
 */
class TgIntegrationHelper {

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The tempstore factory object.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  private $tempStoreFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private $entityTypeManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The module storage service.
   *
   * @var \Drupal\tg_integration\TgIntegrationStorageInterface
   */
  private $storage;

  /**
   * The Telegram bot service.
   *
   * @var \Drupal\tg_integration\TgIntegrationBotInterface
   */
  private $bot;

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * The module tempstore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  private $tempstore;

  /**
   * Constructs a Service object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\tg_integration\TgIntegrationStorageInterface $storage
   *   The Storage service.
   * @param \Drupal\tg_integration\TgIntegrationBotInterface $bot
   *   The bot service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    PrivateTempStoreFactory $temp_store_factory,
    EntityTypeManager $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TgIntegrationStorageInterface $storage,
    TgIntegrationBotInterface $bot,
  ) {
    $this->configFactory = $config_factory;
    $this->tempStoreFactory = $temp_store_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->storage = $storage;
    $this->bot = $bot;
    $this->config = $this->configFactory->get('tg_integration.settings');
    $this->tempstore = $this->tempStoreFactory->get('tg_integration_collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getContentEntities():array {
    $entity_types = [];

    // 'Internal' content entities
    $skipped_entity_types = [
      'block_content',
      'media',
      'menu_link_content',
      'redirect',
      'shortcut',
      'user',
    ];

    $entity_type_definitions = $this->entityTypeManager->getDefinitions();
    foreach ($entity_type_definitions as $definition) {
      if ($definition instanceof ContentEntityType) {
        $entity_id = $definition->id();
        $entity_link = $definition->hasLinkTemplate('canonical');
        $entity_bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_id);
        if ($entity_link && $entity_bundles && !in_array($entity_id, $skipped_entity_types)) {
          $entity_types[$entity_id] = array_keys($entity_bundles);
        }
      }
    }

    return $entity_types;
  }

  /**
   * {@inheritdoc}
   */
  public function isCheckedEntity($entity_type, $bundle): bool {
    $content_types = $this->config->get('content_types') ?? [];
    $current_type = $entity_type . '|' . $bundle;
    return in_array($current_type, array_filter($content_types));
  }

  /**
   * {@inheritdoc}
   */
  public function getCheckedEntities(): array {
    $checked_entities = [];
    $entities = $this->config->get('content_types') ?? [];
    $entities = array_filter($entities);
    foreach ($entities as $entity) {
      $parts = explode("|", $entity);
      $checked_entities[] = [
        'type' => $parts[0],
        'bundle' => $parts[1],
      ];
    }

    return $checked_entities;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntitiesOptions(): array {
    $options = [];
    $entities = self::getContentEntities();
    foreach ($entities as $entity => $bundles) {
      foreach ($bundles as $bundle) {
        $options[$entity . '|' . $bundle] = ucfirst($bundle) . ' (' . $entity . ')';
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function processEntity($entity) {

    // Alter only 'add' and 'edit' forms for bundles from module's settings.
    if (!self::isCheckedEntity($entity->getEntityTypeId(), $entity->bundle())) {
      return;
    }
    $entity_type = $entity->getEntityTypeId();
    $entity_id = $entity->id();
    $post_to_tg = $this->tempstore->get('post_to_tg');
    $comments_status_form = $this->tempstore->get('display_comments');
    $comments_status_db = $this->storage->getCommentsStatus($entity_type, $entity_id);

    if (!$post_to_tg && !is_null($comments_status_db)) {
      if ($comments_status_db != $comments_status_form) {
        $this->storage->changeCommentsStatus($entity_type, $entity_id, $comments_status_form);
      }
      return;
    }

    $hasTgPost = $this->storage->hasTgPost($entity_type, $entity_id);

    if (!$hasTgPost && $post_to_tg) {
      $message = $this->tempstore->get('tg_message_text');
      if (!$message) {
        $message = $entity->getTitle();
      }
      $entity_alias = $entity->toUrl($rel = 'canonical',
        $options = ['absolute' => TRUE])->toString();
      $parameters = [
        // @todo Add automatic tags to the message from the entity.
        'text' => $message . "\n" . $entity_alias,
        'disable_notification' => $this->config->get('silent_mode'),
        'disable_web_page_preview' => $this->config->get('disable_link_preview'),
      ];

      $message_id = $this->bot->sendMessage($parameters);
      if ($message_id) {
        $this->storage->setTgPost($entity_type, $entity_id, $message_id, $comments_status_form);
      }
      $this->tempstore->delete('tg_message_text');
    }
    else {
      $this->storage->setTgPost($entity_type, $entity_id, 0, FALSE);
    }
  }

}
