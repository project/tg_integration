Telegram integration module.
---------------------------

* Introduction
* Configuration
* Maintainers

INTRODUCTION
------------

Automatically posting nodes or other content entities to the telegram channel.
Displaying post's comments from the telegram channel under the nodes.

CONFIGURATION
------------

1. Create a Public Channel in the Telegram (Menu -> New Channel).
    Fill with the name of the channel and short name in the channel's link.
    The channel's image and description are optional.
2. Copy the name from the channel link (after t.me/) and insert it
    in the field 'Chat name / Chat ID' bellow.
3. Alternatively, you can use chat Id without trailing "-100". The simplest way
    find this number copy link to any message from this chat. For example
    https://t.me/c/2230488344/723. "2230488344" - is needed number the field
    'Chat name / Chat ID'.
4. Create new telegram bot. For this, find contact 'BotFather' in the Telegram
    and send string '/newbot' in the conversation. Follow to the instruction.
    Give a user-friendly name and a short name to your bot.
    Details: https://core.telegram.org/bots#3-how-do-i-create-a-bot
5. Receive token to access the HTTP API and insert it to the field
    'Bot authentication token' bellow.
6. Add newly created bot to your channel as administrator.
7. Fill default posting settings and choose content types for
    crossposting to the Telegram.
8. Clear cache.
9. After this you see new options on the edit form of selected entities.
    The new field 'Tg comments' on the entity display settings will be appeared.
10. That's all. Enjoy.

MAINTAINERS
-----------

* Artem Honcharov (Skymen) - https://www.drupal.org/u/skymen
